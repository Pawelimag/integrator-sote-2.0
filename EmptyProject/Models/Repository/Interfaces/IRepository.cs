﻿using System.Collections.Generic;

namespace AplikacjaSote.Models.Repository.Interfaces
{
    public interface IRepository<out TEntity> where TEntity : class
    {
        IEnumerable<TEntity> Get();

        TEntity GetById(object id);

    }
}
