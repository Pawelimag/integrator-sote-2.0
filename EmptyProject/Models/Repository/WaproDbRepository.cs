﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AplikacjaSote.Models.Models;

namespace AplikacjaSote.Models.Repository
{
    class WaproDbRepositoryWaproDb : Repository<WAPRODB>
    {
        public DatabaseClientEntities context;
        public DbSet<DatabaseClientEntities> dbSet;

        public WaproDbRepositoryWaproDb(DatabaseClientEntities context) : base(context)
        {
            this.context = context;
            this.dbSet = context.Set<DatabaseClientEntities>();
        }

        public string GetDbId()
        {
            return Context.WAPRODBs.FirstOrDefault()?.WAPRODB_ID.ToString();
        }

    }

    class WaproDbRepositoryArtykul : Repository<WAPRODB>
    {
        public DatabaseClientEntities context;
        public DbSet<DatabaseClientEntities> dbSet;

        public WaproDbRepositoryArtykul(DatabaseClientEntities context) : base(context)
        {
            this.context = context;
            this.dbSet = context.Set<DatabaseClientEntities>();
        }
        
        public IEnumerable<decimal> GetIds()
        {
            return Context.ARTYKULs.Select(o => o.ID_ARTYKULU).ToList();
        }

        public ARTYKUL GetArticle(decimal id)
        {
            return Context.ARTYKULs.FirstOrDefault(o => o.ID_ARTYKULU == id);
        }

        public ARTYKUL GetArtykul(string indexOrName)
        {
            return Context.ARTYKULs.FirstOrDefault(o => o.INDEKS_KATALOGOWY == indexOrName || o.NAZWA == indexOrName);
        }
    }

    class WaproDbRepositorySoteOrders : Repository<SOTE_ORDERS>
    {
        public DatabaseClientEntities context;
        public DbSet<DatabaseClientEntities> dbSet;

        public WaproDbRepositorySoteOrders(DatabaseClientEntities context) : base(context)
        {
            this.context = context;
            this.dbSet = context.Set<DatabaseClientEntities>();
        }

        public IEnumerable<SoteOrders> GetAllConnecterSoteOrders()
        {
            return Context.SOTE_ORDERS.Select(orders => new SoteOrders {Id = orders.ID, Number = orders.NUMBER}).ToList();
        }

    }
}
