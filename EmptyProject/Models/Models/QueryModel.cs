﻿namespace AplikacjaSote.Models.Models
{
    public class QueryModel
    {

        private static string CreateStringAlterTable(string tableName, string field, string properties)
        {
            return
                $@" --dodajemy {field}
                           IF NOT EXISTS (
                             SELECT *
                             FROM   sys.columns
                             WHERE  object_id = OBJECT_ID(N'{tableName}') AND name = '{field}'
                            )
                            BEGIN
                                    ALTER TABLE {tableName}
                                    ADD {field} {properties}
                            END ";
        }

       
    //    public static string CreateEmptyProjectHistory()
    //    {
    //        var query = @"IF (not EXISTS (SELECT * 
    //            FROM INFORMATION_SCHEMA.TABLES 
    //            WHERE TABLE_SCHEMA = 'dbo' 
    //            AND  TABLE_NAME = '_IMAG_EmptyProject_HISTORY'))
    //            BEGIN
    //                create table _IMAG_EmptyProject_HISTORY(
    //                    [id] int IDENTITY(1,1) PRIMARY KEY NOT NULL,
    //                    [id_kontrahenta] int  NOT NULL,
    //                    [data] datetime  NOT NULL,
    //                    [id_szablonu] int  NULL,
    //                    [numer_dokumentu] varchar(50)  NULL,
    //                    FOREIGN KEY (id_szablonu) REFERENCES _IMAG_EmptyProject_SZABLONY(id)
    //            )
    //            END";

    //        query += CreateStringAlterTable("_IMAG_EmptyProject_HISTORY", "id_kontrahenta", "int NOT NULL");


    //        return query;
    //    }
    }
}
