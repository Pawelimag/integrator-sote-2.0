﻿using System.Collections.Generic;
using AplikacjaSote.Extensions;

namespace AplikacjaSote.Models.Models
{
    public class GlobalSettingsModel : BindableBaseImpl
    {
        private List<string> _allTypy = new List<string>() { "Windows Authentication", "SQL Server Authentication" };

        private int _infoMailBefore = 3;
        private int _infoMailAfter = 0;
        private int _infoMailAfterRepeat = 1;
        private decimal _sumaNiezaplaconychFaktur = 0;
        private int _liczbaKontrahentow = 0;
        private int _liczbaDokumentow = 0;
        private int _rowPerPage;
        private string _numerLicencji = Settings.Default.vNumerLicencji;
        private string _dataWygasnieciaLicencji;

        public static string TextBrakLicencji = "Brak licencji - nie można wykonać zadania!";

        public List<string> AllTypy
        {
            get { return _allTypy; }
            set
            {
                _allTypy = value;
                OnPropertyChanged(() => AllTypy);
            }
        }

        public int InfoMailBefore
        {
            get { return _infoMailBefore; }
            set
            {
                _infoMailBefore = value;
                OnPropertyChanged(() => InfoMailBefore);
            }
        }

        public int InfoMailAfter
        {
            get { return _infoMailAfter; }
            set
            {
                _infoMailAfter = value;
                OnPropertyChanged(() => InfoMailAfter);
            }
        }

        public int InfoMailAfterRepeat
        {
            get { return _infoMailAfterRepeat; }
            set
            {
                _infoMailAfterRepeat = value;
                OnPropertyChanged(() => InfoMailAfterRepeat);
            }
        }

        public decimal SumaNiezaplaconychFaktur
        {
            get { return _sumaNiezaplaconychFaktur; }
            set
            {
                _sumaNiezaplaconychFaktur = value;
                OnPropertyChanged(() => SumaNiezaplaconychFaktur);
            }
        }
        
         public int LiczbaKontrahentow
        {
            get { return _liczbaKontrahentow; }
            set
            {
                _liczbaKontrahentow = value;
                OnPropertyChanged(() => LiczbaKontrahentow);
            }
        }


        public int LiczbaDokumentow
        {
            get { return _liczbaDokumentow; }
            set
            {
                _liczbaDokumentow = value;
                OnPropertyChanged(() => LiczbaDokumentow);
            }
        }

        public int RowPerPage
        {
            get { return _rowPerPage; }
            set
            {
                _rowPerPage = value;
                OnPropertyChanged(() => RowPerPage);
            }
        }

        public string NumerLicencji
        {
            get { return _numerLicencji; }
            set
            {
                _numerLicencji = value;
                OnPropertyChanged(() => NumerLicencji);
                Settings.Default.vNumerLicencji = NumerLicencji;
                Settings.Default.Save();
            }
        }

        public string DataWygasnieciaLicencji
        {
            get { return _dataWygasnieciaLicencji; }
            set
            {
                _dataWygasnieciaLicencji = value;
                OnPropertyChanged(() => DataWygasnieciaLicencji);
            }
        }
    }
}
