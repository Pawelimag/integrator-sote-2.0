﻿using System;
using System.Windows;
using AplikacjaSote.Extensions;
using AplikacjaSote.Views;

namespace AplikacjaSote
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            try
            {
                if (e.Args.Length > 0)
                {

                    if (!e.Args[0].Contains("/Auto")) return;

                    //TODO ...

                    Environment.Exit(1);
                }
                else
                {
                    var mainWindow = new MainWindow();
                    mainWindow.Show();
                }
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                Environment.Exit(1);
            }
        }
        
    }
}