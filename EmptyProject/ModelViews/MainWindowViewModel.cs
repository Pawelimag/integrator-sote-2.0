﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using AplikacjaSote.Controllers;
using AplikacjaSote.Extensions;
using AplikacjaSote.Models.Models;
using AplikacjaSote.Views;
using MaterialDesignThemes.Wpf;

namespace AplikacjaSote.ModelViews
{
    public sealed class MainWindowViewModel : BindableBaseImpl
    {
        /// <summary>
        /// VERSION APP
        /// </summary>
        public const string _versionApp = "v1.0.0";

        #region Fields

        private DataBaseController _dataBaseConnection;
        private SoteController _soteConnection;
        private IEnumerable<SoteOrders> _soteOrders;
        private bool _isTaskOpen;
        private bool _isActiveTaskScheduler;
        private object _taskContent;

        private DateTime _timeStartScheduler;
        private int _currentPageMonitor = 1;
        private int _allPageMonitor = 1;
        private bool _isLicense;
        private LicenseManagerController.LicencjaType _licenseType;

        private const string NameTaskScheduler = "Integrator Sote";

        #endregion

        #region Get;Set;

        public GlobalSettingsModel GlobalSettingsModel { get; set; } = new GlobalSettingsModel();

        public DataBaseController DataBaseConnection
        {
            get { return _dataBaseConnection; }
            set
            {
                _dataBaseConnection = value;
                OnPropertyChanged();
            }
        }

        public IEnumerable<SoteOrders> SoteOrder
        {
            get { return _soteOrders; }
            set
            {
                _soteOrders = value;
                OnPropertyChanged();
            }
        }

    public SoteController SoteConnetion
        {
            get { return _soteConnection; }
            set
            {
                _soteConnection = value;
                OnPropertyChanged();
            }
        }

        public bool IsTaskOpen
        {
            get { return _isTaskOpen; }
            set
            {
                if (_isTaskOpen == value) return;
                _isTaskOpen = value;
                OnPropertyChanged();
            }
        }

        public bool IsLicense
        {
            get { return _isLicense; }
            set
            {
                if (_isLicense == value) return;
                _isLicense = value;
                OnPropertyChanged();
            }
        }

        public object TaskContent
        {
            get { return _taskContent; }
            set
            {
                if (_taskContent == value) return;
                _taskContent = value;
                OnPropertyChanged();
            }
        }
        
        public LicenseManagerController.LicencjaType LicenseType
        {
            get { return _licenseType; }
            set
            {
                if (_licenseType == value) return;
                _licenseType = value;
                OnPropertyChanged();
            }
        }
        
        public DateTime TimeStartScheduler
        {
            get { return _timeStartScheduler; }
            set
            {
                _timeStartScheduler = value;
                OnPropertyChanged(() => TimeStartScheduler);
            }
        }

        public bool IsActiveTaskScheduler
        {
            get { return _isActiveTaskScheduler; }
            set
            {
                _isActiveTaskScheduler = value;
                OnPropertyChanged(() => IsActiveTaskScheduler);
            }
        }

        public int CurrentPageMonitor
        {
            get { return _currentPageMonitor; }
            set
            {
                _currentPageMonitor = value;
                OnPropertyChanged(() => CurrentPageMonitor);
            }
        }

        public int AllPageMonitor
        {
            get { return _allPageMonitor; }
            set
            {
                _allPageMonitor = value;
                OnPropertyChanged(() => AllPageMonitor);
            }
        }

        public string VersionApp => _versionApp;

        #endregion

        #region Icommand

        public ICommand SaveDatabaseData { get; set; }   
        public ICommand CreateTask { get; set; }
        public ICommand RemoveTask { get; set; }
        public ICommand ConnectDatabase { get; set; }
        public ICommand ConnectSote { get; set; }        
        public ICommand CheckLicenseButton { get; set; }
        public ICommand TestButtonAction { get; set; }
        public ICommand Checker { get; set; }
        public ICommand SendOrderToMag { get; set; }

        #endregion

        public MainWindowViewModel()
        {
            var activeTask = TaskSchedulerController.GetTask(NameTaskScheduler);

            _dataBaseConnection = DataBaseController.Instance;
            _soteConnection = SoteController.Instance;
            _soteOrders = SoteController.GetConnectedSoteOrders().ToList();
            _timeStartScheduler = activeTask?.NextRunTime ?? DateTime.Now;
            _isActiveTaskScheduler = activeTask != null && activeTask.Enabled;

            InitBindings();
            SetLicenseInit();
        }

        private void SetLicenseInit()
        {
            DateTime? dataWygasnieciaLicencji;
            IsLicense = LicenseManagerController.GetLicense(GlobalSettingsModel.NumerLicencji, out _licenseType, out dataWygasnieciaLicencji);

            GlobalSettingsModel.DataWygasnieciaLicencji = dataWygasnieciaLicencji?.ToLongDateString() ?? "-";
        }

        private void InitBindings()
        {
            SaveDatabaseData = new AwaitableDelegateCommand(SaveDatabaseDataAction);          
            CreateTask = new AwaitableDelegateCommand(CreateTaskAction);
            RemoveTask = new AwaitableDelegateCommand(RemoveTaskAction);
            ConnectDatabase = new AwaitableDelegateCommand(ConnectDatabaseAction);
            ConnectSote = new AwaitableDelegateCommand(ConnectToSoteAction);
            TestButtonAction = new AwaitableDelegateCommand(TestButtonAsync);
            CheckLicenseButton = new AwaitableDelegateCommand(CheckLicenseButtonAction);
            Checker = new AwaitableDelegateCommand(CheckerAction);
            SendOrderToMag = new AwaitableDelegateCommand(SendOrderTogAction);
        }
        
        #region Actions
        
        private async Task SaveDatabaseDataAction()
        {
            var resp = DataBaseController.SaveDatabaseData(DataBaseConnection);
            DataBaseController.Reset();

            await ShowMessagebox(resp ? "Baza danych została zapisana" : "Wystąpił błąd!");
        }
              
        private async Task CreateTaskAction() => await CreateTaskAsync();
        private async Task RemoveTaskAction() => await RemoveTaskAsync();
        private async Task ConnectDatabaseAction() => await ConnectDatabaseAsync();
        private async Task ConnectToSoteAction() => await ConnectToSoteAsynch();        
        private async Task CheckLicenseButtonAction() => await CheckLicenseButtonActionAsync();
        private async Task CheckerAction() => await CheckerActionAsync();
        private async Task SendOrderTogAction() => await SendOrderToMagAsync();

        #endregion


        #region TasksAsync

        public async Task CreateTaskAsync()
        {
            try
            {
                if (!IsLicense)
                    throw new Exception(GlobalSettingsModel.TextBrakLicencji);
                
                await Task.Factory.StartNew(() =>
                {
                   var taskCreate = TaskSchedulerController.CreateTask(NameTaskScheduler, "Empty projekt", TimeStartScheduler.Hour, TimeStartScheduler.Minute);
                    if (taskCreate)
                        IsActiveTaskScheduler = true;
                });
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                await ShowMessagebox(ex.Message);
            }
        }

        public async Task RemoveTaskAsync()
        {
            try
            {
                if (!IsLicense)
                    throw new Exception(GlobalSettingsModel.TextBrakLicencji);
                
                await Task.Factory.StartNew(() =>
                {
                    var taskCreate = TaskSchedulerController.RemoveTask(NameTaskScheduler);
                    if (taskCreate)
                        IsActiveTaskScheduler = false;
                });
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                await ShowMessagebox(ex.Message);
            }
        }
        
        public async Task ConnectDatabaseAsync()
        {
            var isConnected = false;
            try
            {
                OpenTask(new DialogLoading());

                await Task.Factory.StartNew(() =>
                {
                    if (DataBaseController.Instance == null || !DataBaseController.Instance.CreateTables())
                        throw new Exception("Wystąpił błąd podczas tworzenia tabel w bazie danych");

                    isConnected = true;
                });
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                await ShowMessagebox(ex.Message);
            }
            finally
            {
                ClosingTask();
            }

            if(isConnected)
                await ShowMessagebox(@"Baza danych jest gotowa do pracy");
        }

        public async Task SendOrderToMagAsync()
        {
            var isConnected = false;
            try
            {
                OpenTask(new DialogLoading());

                await Task.Factory.StartNew(() =>
                {
                    if (SoteController.Instance == null || SoteController.Instance.GetOrdersToMag() == false)
                        throw new Exception("Wystąpił błąd podczas pobierania zamówień");

                    isConnected = true;
                });
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                await ShowMessagebox(ex.Message);
            }
            finally
            {
                ClosingTask();
            }

            if (isConnected)
                await ShowMessagebox(@"Zamówienia zostały pobrane");
        }

        public async Task CheckerActionAsync()
        {
            try
            {
                OpenTask(new Checker());

                await Task.Factory.StartNew(() =>
                {
                    Thread.Sleep(5000);
                });

            }
            catch
            {
                ClosingTask();
            }
        }

        public async Task ConnectToSoteAsynch()
        {
            OpenTask(new DialogLoading());
            try
            {
                await Task.Factory.StartNew(() =>
                {
                    if (SoteController.Instance == null || !SoteController.Instance.CheckConnection())
                        throw new Exception("Brak połączenia z serwisem Sote.");
                });
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                await ShowMessagebox(ex.Message);
            }
            finally
            {
                ClosingTask();
            }

        }



        public static async Task ShowMessagebox(string message)
        {
            var sampleMessageDialog = new MessageBoxDialog
            {
                Message =
                {
                    Text = message,
                    MinWidth = 150
                }
            };

            await DialogHost.Show(sampleMessageDialog, "RootDialog");
        }
        
      
        public async Task CheckLicenseButtonActionAsync()
        {
            var respDesc = string.Empty;
            DateTime? respDataWygasniecia = null;
            try
            {
                OpenTask(new DialogLoading());

                await Task.Factory.StartNew(() =>
                {
                    LicenseManagerController.LicencjaType licencjaType;

                    IsLicense = LicenseManagerController.ActiveLicense(GlobalSettingsModel.NumerLicencji, out licencjaType, out respDesc, out respDataWygasniecia);
                    LicenseType = licencjaType;
                    GlobalSettingsModel.DataWygasnieciaLicencji = respDataWygasniecia?.ToLongDateString() ?? "-";
                });
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
            }
            finally
            {
                ClosingTask();
            }

            var dataWygasniecia = string.Empty;
            if (respDataWygasniecia != null)
                dataWygasniecia = $"Data wygaśnięcia: {respDataWygasniecia.Value.ToLongDateString()}";
                
            await ShowMessagebox(IsLicense ? $@"{respDesc}{Environment.NewLine}{dataWygasniecia}" : $"Brak licencji: {respDesc}");
        }
        
        public async Task CheckLicenseButtonActionWithoutDialogAsync()
        {
            try
            {
                await Task.Factory.StartNew(() =>
                {
                    LicenseManagerController.LicencjaType licencjaType;
                    DateTime? dataWygasniecia;
                    IsLicense = LicenseManagerController.GetLicense(GlobalSettingsModel.NumerLicencji, out licencjaType,out dataWygasniecia);
                    LicenseType = licencjaType;
                    GlobalSettingsModel.DataWygasnieciaLicencji = dataWygasniecia?.ToLongDateString() ?? "-";
                });
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
            }
        }

        public async Task TestButtonAsync()
        {
            try
            {
                await ShowMessagebox(@"Wiadomość testowa - przykład działania");
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
            }
        }
        #endregion


        public DialogClosingEventHandler ClosingEventHandler { get; set; }

        private static void ExtendedClosingEventHandler(object sender, DialogClosingEventArgs eventArgs)
        {
            // Method intentionally left empty.
        }

        public void OpenTask(object taskContent)
        {
            TaskContent = taskContent;
            IsTaskOpen = true;
        }

        public void ClosingTask()
        {
            IsTaskOpen = false;
        }
    }
}
