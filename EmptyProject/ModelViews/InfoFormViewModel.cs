﻿using AplikacjaSote.Extensions;
using AplikacjaSote.Models.Models;

namespace AplikacjaSote.ModelViews
{
    public sealed class InfoFormViewModel : BindableBaseImpl
    {
        private WaproMag _waproMag;

        public InfoFormViewModel()
        {
            _waproMag = new WaproMag
            {
                Version = 1.6
            };
            InitBindings();
        }

        public WaproMag Wapro
        {
            get { return _waproMag; }
            set
            {
                _waproMag = value;
                OnPropertyChanged();
            }
        }

        private void InitBindings()
        {

        }
    }
}
