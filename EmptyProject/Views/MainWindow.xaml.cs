﻿using System.Windows;

namespace AplikacjaSote.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void tabControl_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {

        }
        
        private void cbDatabaseAuth_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            var selectBox = cbDatabaseAuth.SelectionBoxItem.ToString();
            if (selectBox != null && selectBox.Contains("SQL"))
            {
                txtDatabaseLogin.IsEnabled = false;
                txtDatabaseLogin.Text = string.Empty;
                txtDatabasePassword.IsEnabled = false;
                txtDatabasePassword.Text = string.Empty;
            }
            else
            {
                txtDatabaseLogin.IsEnabled = true;
                txtDatabasePassword.IsEnabled = true;
            }
        }

        private void cbActiveTaskScheduler_Checked(object sender, RoutedEventArgs e)
        {
            if (cbActiveTaskScheduler.IsChecked != null && btnRemoveTashScheduler != null)
            {
                btnRemoveTashScheduler.IsEnabled = (bool)cbActiveTaskScheduler.IsChecked;
                
            }

            btnCreateTask.Content = cbActiveTaskScheduler.IsChecked != null && (bool) cbActiveTaskScheduler.IsChecked
                ? "Modyfikuj zadanie"
                : "Utwórz zadanie";
        }

    }
}
