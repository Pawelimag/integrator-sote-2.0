﻿using System;

namespace AplikacjaSote.Extensions
{
    public static class DateHelper
    {
        public static DateTime InTtoDate(int data)
        {
            if (data <= 36163) return new DateTime();

            try
            {
                var d = data - 36163;
                var d2 = new DateTime(1900, 1, 1);
                d2 = d2.AddDays(d);

                return d2;
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                return new DateTime();
            }
        }

        public static int DateToInt(DateTime data)
        {
            try
            {
                var start = new DateTime(1900, 1, 1);
                var dif = (data - start).TotalDays;
                dif = dif + 36163;
                return Convert.ToInt32(dif);
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                return 0;
            }
        }
    }
}
