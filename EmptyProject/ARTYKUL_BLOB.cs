//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AplikacjaSote
{
    using System;
    using System.Collections.Generic;
    
    public partial class ARTYKUL_BLOB
    {
        public decimal ID_BLOBA { get; set; }
        public string INDEKS_KATALOGOWY { get; set; }
        public byte[] ZDJECIE { get; set; }
        public string NAZWA { get; set; }
        public Nullable<byte> DOMYSLNE { get; set; }
        public Nullable<System.Guid> GUID_ARTYKUL_BLOB { get; set; }
        public byte[] ZDJECIE_TMP { get; set; }
    }
}
