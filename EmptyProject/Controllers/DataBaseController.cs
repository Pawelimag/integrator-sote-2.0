﻿using System;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using AplikacjaSote.Extensions;

namespace AplikacjaSote.Controllers
{
    public sealed class DataBaseController
    {
        public static DataBaseController Instance { get; set; } = new DataBaseController();

        private string _auth = Settings.Default.dbAuth;
        private string _login = Settings.Default.dbLogin;
        private string _password = Settings.Default.dbPass;
        private string _dbServerName = Settings.Default.dbServerName;
        private string _dbName = Settings.Default.dbDatabaseName;

        public string Auth
        {
            get { return _auth; }
            set
            {
                _auth = value;
                Settings.Default.dbAuth = Auth;
                Settings.Default.Save();
            }
        }
        public string Login
        {
            get { return _login; }
            set
            {
                _login = value;
                Settings.Default.dbLogin = Login;
                Settings.Default.Save();
            }
        }
        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                Settings.Default.dbPass = Password;
                Settings.Default.Save();
            }
        }

        public string DbServerName
        {
            get { return _dbServerName; }
            set
            {
                _dbServerName = value;
                Settings.Default.dbServerName = DbServerName;
                Settings.Default.Save();
            }
        }

        public string DbName
        {
            get { return _dbName; }
            set
            {
                _dbName = value;
                Settings.Default.dbDatabaseName = DbName;
                Settings.Default.Save();
            }
        }
       

        private DataBaseController()
        {
        }
      
        public string DbConnection()
        {
            try
            {
                var uwierzytelnienie = !Auth.Contains("SQL") ? "Integrated Security=true;" : $"User Id={Login};Password={Password};";

                string connString =
                    $@"data source={DbServerName};initial catalog={DbName};{uwierzytelnienie}MultipleActiveResultSets=True;App=EntityFramework; ";

                var esb = new EntityConnectionStringBuilder
                {
                    Metadata = "res://*/DatabaseModel.csdl|res://*/DatabaseModel.ssdl|res://*/DatabaseModel.msl",
                    Provider = "System.Data.SqlClient",
                    ProviderConnectionString = connString
                };
                return esb.ToString();
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
            }
            return "";
        }

        public string CreateConnStrWithoutEntity()
        {
            var windowsAuthString = "Windows Authentication";
            var uwierzytelnienie = !Auth.Contains("SQL") ? windowsAuthString : $"User Id={Login};Password={Password};";

            var auth = uwierzytelnienie == windowsAuthString ? "Integrated Security=true;" :$"User Id={Login};Password={Password};";
          
     
            return $"Data Source={DbServerName};Initial Catalog={DbName};{auth}";
        }

        public static bool SaveDatabaseData(DataBaseController dbObject)
        {
            try
            {
                Settings.Default.dbServerName = dbObject.DbServerName;
                Settings.Default.dbDatabaseName = dbObject.DbName;
                Settings.Default.dbAuth = dbObject.Auth;
                Settings.Default.dbLogin = dbObject.Login;
                Settings.Default.dbPass = dbObject.Password;

                Settings.Default.Save();

                return true;
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                return false;
            }
           
        }

        public static void Reset() {
            Instance = new DataBaseController();
        }

        public bool RunQueryWithoutEntity(string query)
        {
            try
            {
                using (var conn = new SqlConnection(CreateConnStrWithoutEntity()))
                {
                    conn.Open();
                    using (var cmd = new SqlCommand(query, conn))
                    {
                        if (query != null)
                            cmd.ExecuteNonQuery();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Logs.AddLogError(new Exception("QUERY:" + query + ";" + ex));
                return false;
            }
        }

        public bool CreateTables()
        {
            //TODO IT'S EXAMPLE

           // if (!RunQueryWithoutEntity(QueryModel.CreateEmptyProjectHistory()))
              //  boolReq = false;


            return true; //CHANGE TO FALSE!!! it's for test connection to sql db
        }
    }
}
