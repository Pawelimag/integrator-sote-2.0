﻿using System;
using AplikacjaSote.Extensions;
using AplikacjaSote.Models.Repository;

namespace AplikacjaSote.Controllers
{
    public class LicenseManagerController
    {
        public static bool GetLicense(string numerLicencji, out LicencjaType licencjaType, out DateTime? dataWygasniecia)
        {
            try
            {
                var licencja = new service {Url = "http://195.160.180.44/LicenseManager.svc?wsdl"};

                var wfmagDbId = GetWfMagDbNumber();

                var response = licencja.GetLicense(numerLicencji, wfmagDbId, "INTEGRATOR_SOTE");

                licencjaType = response != null && response.Response ? LicencjaType.Potwierdzona : LicencjaType.Brak;
                dataWygasniecia = response != null && response.DataWygasnieciaSpecified
                    ? response.DataWygasniecia
                    : null;
                return response != null && response.Response;
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                licencjaType = LicencjaType.Tymczasowa;
                dataWygasniecia = null;
                return true;
            }
         
        }

        public static bool ActiveLicense(string numerLicencji, out LicencjaType licencjaType, out string licencjaResponseDesc, out DateTime? licencjaResponseDataWygasniecia)
        {
            try
            {
                var licencja = new service { Url = "http://195.160.180.44/LicenseManager.svc?wsdl" };

                var wfmagDbId = GetWfMagDbNumber();

                var response = licencja.ActiveLicense(numerLicencji, wfmagDbId, "INTEGRATOR_SOTE");

                licencjaType = response != null && response.Response ? LicencjaType.Potwierdzona : LicencjaType.Brak;

                licencjaResponseDesc = response?.Description;
                licencjaResponseDataWygasniecia = response != null && response.DataWygasnieciaSpecified ? response.DataWygasniecia:null;

                return response != null && response.Response;
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                licencjaType = LicencjaType.Tymczasowa;
                licencjaResponseDesc = "Brak połączenia z API";
                licencjaResponseDataWygasniecia = null;
                return true;
            }
        }

        private static string GetWfMagDbNumber()
        {
            try
            {
                var getWfMagDbNumber = new WaproDbRepositoryWaproDb(new DatabaseClientEntities(DataBaseController.Instance.DbConnection()));
                var wfmagDbId = getWfMagDbNumber.GetDbId();
                return wfmagDbId;
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                return string.Empty;
            }
          
        }

        public enum LicencjaType
        {
            Potwierdzona,
            Brak,
            Tymczasowa
        }
    }
}
