﻿using System;
using Microsoft.Win32.TaskScheduler;
using Task = Microsoft.Win32.TaskScheduler.Task;

namespace AplikacjaSote.Controllers
{
    public static class TaskSchedulerController
    {
        public static bool CreateTask(string subjectTask, string descriptionTask, int hour, int minutes)
        {
            using (var ts = new TaskService())
            {
                var td = ts.NewTask();
                td.RegistrationInfo.Description = descriptionTask;
                
                td.Triggers.Add(new DailyTrigger
                {
                    DaysInterval = 1,
                    StartBoundary = DateTime.Today.AddHours(hour).AddMinutes(minutes),
                    Enabled = true
                });
                
                td.Actions.Add(new ExecAction(System.IO.Directory.GetCurrentDirectory()+ "/EmptyProject.exe", "/Auto", System.IO.Directory.GetCurrentDirectory()));
                
                ts.RootFolder.RegisterTaskDefinition(subjectTask, td);
                
            }

            return true;
        }

        public static bool RemoveTask(string subjectTask)
        {
            using (var ts = new TaskService())
            {
               ts.RootFolder.DeleteTask(subjectTask);
            }

            return true;
        }

        public static Task GetTask(string subjectTask)
        {
            using (var ts = new TaskService())
            {

                return ts.GetTask(subjectTask);

            }
        }


    }
}
