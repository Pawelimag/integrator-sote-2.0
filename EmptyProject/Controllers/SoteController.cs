﻿using System;
using System.Collections.Generic;
using AplikacjaSote.Extensions;
using AplikacjaSote.Models.Models;
using AplikacjaSote.Models.Repository;

namespace AplikacjaSote.Controllers
{
    public sealed class SoteController
    {
        public static SoteController Instance { get; set; } = new SoteController();

        private readonly SoteLogin.stWebApiBackendPortTypeClient _sote = new SoteLogin.stWebApiBackendPortTypeClient();
        private readonly stOrderPortTypeClient _soteOrder = new stOrderPortTypeClient();
        public string Hash;

        private string _login = Settings.Default.SoteLogin;
        private string _password = Settings.Default.SotePassword;

        public string Login
        {
            get { return _login; }
            set
            {
                _login = value;
                Settings.Default.SoteLogin = Login;
                Settings.Default.Save();
            }
        }

        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                Settings.Default.SotePassword = Password;
                Settings.Default.Save();
            }
        }

        private SoteController()
        {
        }

        public static void Reset()
        {
            Instance = new SoteController();
        }
        public bool CheckConnection()
        {
            var userLogin = new SoteLogin.doLogin()
            {
                password = Password,
                username = Login
            };

            try
            {
                var result = _sote.doLogin(userLogin);
                Hash = result.hash;
            }
            catch
            {
                Hash = null;
            }

            return Hash != null;
        }

        public GetOrderResponse GetOrder(string id)
        {
            var newOrder = new GetOrder
            {
                _session_hash = Hash,
                id = id
            };
            try
            {
                var order = _soteOrder.GetOrder(newOrder);
                return order;
            }
            catch
            {
                return null;
            }
        }

        public static IEnumerable<SoteOrders> GetConnectedSoteOrders()
        {
            try
            {
                var getSoteOrdersFromDb =
                    new WaproDbRepositorySoteOrders(
                        new DatabaseClientEntities(DataBaseController.Instance.DbConnection()));
                return getSoteOrdersFromDb.GetAllConnecterSoteOrders();
            }
            catch (Exception ex)
            {
                Logs.AddLogError(ex);
                return new List<SoteOrders>();
            }
        }

        public bool GetOrdersToMag()
        {
            var orders = GetAllOrder();
            return orders != null;
        }

        private GetOrderListResponse[] GetAllOrder()
        {
            if(Hash == null)
                CheckConnection();

            var orderGet = new GetOrderList
            {
                _modified_from = DateTime.Now.AddDays(-1),
                _modified_to = DateTime.Now.AddDays(1),
                _session_hash = Hash
            };
            try
            {
                var ordersList = _soteOrder.GetOrderList(orderGet);
                return ordersList;
            }
            catch(Exception ex)
            {
                return null;
            }
        }


    }
}
